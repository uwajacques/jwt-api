<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'api'], function () {
   
	Route::post('/v1/login', 'AuthCtrl@auth'); //done 
	Route::post('/v1/logout', 'AuthCtrl@logout');  //done 
	Route::get('/v1/users', 'MainCtrl@index');  //done 
	Route::get('/v1/user/{id}', 'MainCtrl@show'); //done 
	Route::put('/v1/user/{id}/edit', 'MainCtrl@update');  // done
	Route::delete('/v1/user/{id}/delete', 'MainCtrl@destroy'); //done 
	Route::post('/v1/user/create', 'MainCtrl@store');  //done

	Route::get('/v1/getusers', 'MainCtrl@importData');
});


