<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests;

class AuthCtrl extends Controller
{
   
   public function __construct()
   {

      $this->middleware('jwt.auth', ['except' => ['auth']]);
   }



   public function auth(Request $request){

   		$credentials = $request->only(['username', 'password']);	

   		try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
   	
   } 

  public function logout()
  {
      $token = JWTAuth::getToken();
      if ($token) {
         $token = JWTAuth::setToken($token)->invalidate();
      }
      return response()->json(['status' => 'success'], 200);  
  }
    
}
